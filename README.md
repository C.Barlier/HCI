# HCI - Hierarchical Cell Identity repository

**This repository is part of a project, linked to the two following repositories:**

**- HCellig R Package: https://github.com/BarlierC/HCellig**

**- HCellig analyses: https://gitlab.com/C.Barlier/HCellig_analyses**

This hierarchical cell identity repository was generated by applying HCellig to the Tabula Muris [1], the Neurons of the Mouse Brain Atlas [2], and the Tabula Sapiens datasets [3]. It reports all identity genes, as well as their level of expression, for every cell type, subtype and phenotype.

*[1] Tabula Muris Consortium et al. Nature 562, 367–372 (2018)*

*[2] La Manno, G. et al. Nature 596, 92–96 (2021)*

*[3] Consortium, T. T. S., The Tabula Sapiens Consortium & Quake, S. R. doi:10.1101/2021.07.19.452956*

